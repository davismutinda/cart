import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CheckoutComponent } from './views/checkout/checkout.component';
import { CollectComponent } from './views/collect/collect.component';
import { ConfirmationComponent } from './views/confirmation/confirmation.component';
import { HomeComponent } from './views/home/home.component';
import { PayComponent } from './views/pay/pay.component';
import { ProductsComponent } from './views/products/products.component';
import { RedeemComponent } from './views/redeem/redeem.component';
import { StoreComponent } from './views/store/store.component';
import { UtilizeComponent } from './views/utilize/utilize.component';

const routes: Routes = [
{
  path:'products/:id',
  component:ProductsComponent
},
{
  path:'store/:id',
  component:StoreComponent
},
{
  path:'payment',
  component:PayComponent
},
{
  path:'redeem/:id',
  component:RedeemComponent
},
{
  path:'collect/:id',
  component:CollectComponent
},
{
  path:'utilize/:id',
  component:UtilizeComponent
},
{
  path:'confirmation',
  component:ConfirmationComponent
},
{
  path:'checkout',
  component:CheckoutComponent
},
{
  path:'',
  component:HomeComponent
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import {MatIconModule} from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { HeaderComponent } from './views/header/header.component';
import { FooterComponent } from './views/footer/footer.component';
import { ProductsComponent } from './views/products/products.component';
import { PayComponent } from './views/pay/pay.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { HomeComponent } from './views/home/home.component';
import { ngxLoadingAnimationTypes, NgxLoadingModule } from 'ngx-loading';
import { ConfirmationComponent } from './views/confirmation/confirmation.component';
import { UtilizeComponent } from './views/utilize/utilize.component';
import { RedeemComponent } from './views/redeem/redeem.component';
import { CheckoutComponent } from './views/checkout/checkout.component';
import { CollectComponent } from './views/collect/collect.component';
import { NgxBraintreeModule } from 'ngx-braintree';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { BackendInterceptor } from './backend.interceptor'
import { GooglePayButtonModule } from '@google-pay/button-angular';
import { NgxSpinnerModule } from "ngx-spinner";
import { StoreComponent } from './views/store/store.component';
import { ModalModule } from 'ngx-bootstrap/modal';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ProductsComponent,
    PayComponent,
    HomeComponent,
    ConfirmationComponent,
    RedeemComponent,
    CollectComponent,
    CheckoutComponent,
    UtilizeComponent,
    StoreComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    MatCardModule,
    MatSelectModule,
    MatExpansionModule,
    NgxBraintreeModule,
    NgxIntlTelInputModule,
    GooglePayButtonModule,
    MatRadioModule,
    NgxSpinnerModule,
    NgxLoadingModule.forRoot({
      animationType: ngxLoadingAnimationTypes.chasingDots,
      backdropBackgroundColour: 'rgba(0,0,0,0.1)', 
      backdropBorderRadius: '4px',
      primaryColour: '#000000', 
      secondaryColour: '#ffda0a', 
      tertiaryColour: '#ffda0a'
  }),
  ModalModule.forRoot()
  ],
  providers: [
    {  
      provide: HTTP_INTERCEPTORS,  
      useClass:BackendInterceptor,  
      multi: true  
    }  
  ],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
})
export class AppModule { }

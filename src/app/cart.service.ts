import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class CartService {
  cart: BehaviorSubject<number> = new BehaviorSubject(0);
  count = this.cart.asObservable()
  cart_products: BehaviorSubject<[]> = new BehaviorSubject([]);
  products = this.cart_products.asObservable()
  cart_items:any=[];
  constructor() {
    if(!sessionStorage.getItem("items")){
     sessionStorage.setItem('items',JSON.stringify([]))
    }else{
      this.cart.next(JSON.parse(sessionStorage.getItem('items')).length)
      this.cart_products.next(JSON.parse(sessionStorage.getItem('items')))
    }
  }

  addCart(data){
    this.cart_items = []
    // const index = this.cart_items.findIndex(obj=>obj.id==data.id)
    // if(index > -1){
    //   let obj = this.cart_items.find(obj=>obj.id == data.id)
    //   this.cart_items.splice(index,1);
    //   this.cart_items.push(obj);
    // }else{
    //   data['quantity']=1
    //   this.cart_items.push(data);
    // }
    this.cart_items.push(data);
    this.cart.next(this.cart_items.length)
    sessionStorage.setItem('items',JSON.stringify(this.cart_items));
    console.log(JSON.parse(sessionStorage.getItem('items')))
    this.cart_products.next(this.cart_items);
   
    }
    removeItem(data){
    this.cart_items = JSON.parse(sessionStorage.getItem('items'));
    const index = this.cart_items.findIndex(obj=>obj.id==data.id)
    this.cart_items.splice(index,1);
    sessionStorage.setItem('items',JSON.stringify(this.cart_items));
    this.cart.next(JSON.parse(sessionStorage.getItem('items')).length)
    this.cart_products.next(this.cart_items);
    }
}

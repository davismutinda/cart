
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
export const endpoint='https://backend.yougotsolutions.com/api/';
@Injectable({
  providedIn: 'root'
})

export class ServiceService {

  constructor(private http: HttpClient) {
  }
  private extractData(res: any) {
    const  body = res;
    // console.log('service response', res);
    return body || { };
  }
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }
  public isAuthenticated(): boolean {
    return true;
  }

  makePayment(data: any): Observable<any> {
    return this.http.post('https://backend.yougotsolutions.com/api/order/g3n3rat30der',data).pipe(
      map(this.extractData));
  }
  checkoutPayment(data: any): Observable<any> {
    return this.http.post('https://mpesa.mykonos.co.ke/v1/braintree/checkout',data).pipe(
      map(this.extractData));
  }

  getProducts(): Observable<any> {
    return this.http.get(endpoint+'products').pipe(
      map(this.extractData));
  }
  storeProducts(id): Observable<any> {
    return this.http.get(endpoint+'store/'+id+'/').pipe(
      map(this.extractData));
  }
  getToken(): Observable<any> {
    return this.http.get('  https://mpesa.mykonos.co.ke/v1/braintree/generate/token').pipe(
      map(this.extractData));
  }
  orderDetails(id){
    return this.http.get('https://backend.yougotsolutions.com/api/ord3r/ch3ckStatus/'+id+'/').pipe(
      map(this.extractData));
  }
  createPurchase(data): Observable<any>{
    return this.http.post('https://mpesa.mykonos.co.ke/v1/braintree/checkout',data).pipe((response: any) => {
           return response;
       });
    }
}

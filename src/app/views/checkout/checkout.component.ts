import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Observable} from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import { ServiceService } from 'src/app/service.service';

declare var braintree: any;
@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit,OnDestroy {
  payments: any[] | undefined;
  showCard: Boolean = true;
  phoneNumber: string =''
  cardNumber: string =''
  ccv: string =''
  cardExpiry: string =''
  selectedObj: any 
  panelOpenState = false;
  submitted=false;
  mpesaForm: FormGroup;
  visaForm: FormGroup;
  id: any;
  total: number;
  cart_items: any=[];
  user:any={};
  loading=false;
  data:any={};
  mpesa=true;
  showCardError: boolean = false;
  enabledStyle = {
  'background-color': '#000000',
  'color': '#ffffff',
  'border': 'none',
  'border-radius': '4px',
  'height': '40px',
  'line-height': '40px',
  'font-size': '16px',
  'cursor': 'pointer'
 };
  codes=['KE']
  cardholdersName: string;
  product: any={};
  mobile;
  totalAngularPackages: any;
  card: any;
  constructor(private formBuilder: FormBuilder,public service:ServiceService,public router:Router,public toastr:ToastrService,public http:HttpClient,private spinner: NgxSpinnerService) {
   this.user= JSON.parse(sessionStorage.getItem('user'))
   if(sessionStorage.getItem('phone')){
    this.mobile =sessionStorage.getItem('phone')
   }
   }

  ngOnInit(): void {
  this.cart_items = JSON.parse(sessionStorage.getItem('items'))
  this.product = this.cart_items[0]
  let amount=0;
  this.cart_items.forEach(element => {
    amount+=parseFloat(element.price)
  });
  this.total = amount;
    this.mpesaForm = this.formBuilder.group({
      phone: ['', Validators.required],

  });
  if(this.mobile){
    this.mpesaForm.patchValue({"phone":this.mobile})
  }
  if(this.user !=undefined && this.user.phone){
    if(this.codes.includes(this.user.phone.countryCode)){
      this.mpesa=false
    }
  }
  this.visaForm = this.formBuilder.group({
    card_number: ['', Validators.required],
    card_expiry: ['', Validators.required],
    cvv: ['', Validators.required]

});
  this.getToken();
  }

  get f() { return this.mpesaForm.controls; }
  get g() { return this.visaForm.controls; }
  getToken(){
    this.service.getToken().subscribe((res)=>{
      this.createUI(res.token);
    })
  }
  onSubmit(){
    if (this.mpesaForm.invalid) {
      return;
    }
    this.spinner.show();
    // let data = {
    //   "merchantID":1,
    //   "amount":1,
    //   "mobileNumber":this.mpesaForm.get('phone')?.value,
    //   "accountNumber":"Test",
    //   "shortcodeType":"PAYBILL"
    // }

    let info = {
      "product_id":this.cart_items[0].id,
      "options":this.cart_items[0].variant_options,
      "name":this.user.name,
      "phone":this.mpesaForm.get('phone')?.value,
      "email":this.user.email,
      "payment_mode": "MPESA",
      "amount": this.total
  }
    // this.user.phone=this.mpesaForm.get('phone')?.value
    // this.user.payment_mode='MPESA'
    // this.user.amount=this.total
    // let order={
    //   "items":this.cart_items,
    //   "user":this.user
    // }
    this.service.makePayment(info).subscribe((res)=>{
      this.spinner.hide();
      setTimeout(()=>this.redirect(res.order_id), 1000);
    })
  }

  redirect(order_no) 
  {
    var count=0;
    this.id =setInterval(()=>{
      count+=1
      this.service.orderDetails(order_no).subscribe((res)=>{
        if(res.payment_status == 1){
          this.toastr.success('Payment Received','Nofication')
          this.loading=false;
            this.router.navigate(['/redeem',order_no])
          // if(res.user.paymentStatus=='CONFIRMED'){
          //   this.toastr.success('Payment Received','Nofication')
          //   this.loading=false;
          //     this.router.navigate(['/redeem',order_no])
          // }
        }
      })
      if(count==5){
        this.ngOnInit();
      }
    },3000)
  } 
  onPaymentStatus(text){
    this.router.navigate(['/redeem',text.transactionID])
  }
  createUI(token){
    braintree.dropin.create({
      authorization:'sandbox_bn7nvd54_85jhqz3kn7xyrtsv',
      container: '#dropin-container',
      applePay: {
        displayName: 'Merchant Name',
        paymentRequest: {
          total: {
            label: 'Localized Name',
            amount: '10.00'
          }
        }
      },
      venmo: true
    }).then((dropinInstance) =>{
      this.card = dropinInstance;

    });

  }
  onLoadPaymentData(event) {
    console.log("load payment data", event.detail);
  }
  
  pay(){
    this.card.requestPaymentMethod().then((payload)=>{
      console.log(payload);
      let info = {
        "product_id":this.cart_items[0].id,
        "options":this.cart_items[0].variant_options,
        "name":this.user.name,
        "phone":this.mpesaForm.get('phone')?.value,
        "email":this.user.email,
        "payment_mode": "CARD",
        "amount": this.total,
        "card_number":payload.details.bin+'******'+payload.details.lastFour,
        "card_exp_month":payload.details.expirationMonth,
        "card_exp_year":payload.details.expirationYear,
        "nonce":payload.nonce
    }
    console.log(info)
    this.service.makePayment(info).subscribe((res)=>{
      this.spinner.hide();
      setTimeout(()=>this.redirect(res.order_id), 1000);
    })
      
//       bin: "411111"
// cardType: "Visa"
// cardholderName: null
// expirationMonth: "02"
// expirationYear: "2025"
// lastFour: "1111"
// lastTwo: "11"
      // let user= JSON.parse(sessionStorage.getItem('user'))
      //   delete user.phone
      //   user.phone=sessionStorage.getItem('phone')
      //   user.payment_mode='CARD'
      //   user.amount = 1
      //   let order={
      //     items:JSON.parse(sessionStorage.getItem('items')),
      //     user:user
      //   }
      //   this.service.checkoutPayment({payement_details:{nonce:payload.nonce,chargeAmount:1},order_details:order}).subscribe((res)=>{
      //   console.log(res);
      //   this.router.navigate(['/redeem',res.transactionID])
      //   })
    })
  }
  ngOnDestroy() {
    if (this.id) {
      clearInterval(this.id);
    }
  }
}

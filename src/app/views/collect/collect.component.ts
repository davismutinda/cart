import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceService } from 'src/app/service.service';
declare var webGlObject1: any;

@Component({
  selector: 'app-collect',
  templateUrl: './collect.component.html',
  styleUrls: ['./collect.component.scss']
})
export class CollectComponent implements OnInit {
  registerForm: any;
  submitted=false;
  product: any={};
  user: any={};
  value: string;
  constructor(private formBuilder: FormBuilder,public service:ServiceService,public route:ActivatedRoute,public router:Router) { }
  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      phone: ['', Validators.required],
      amount: [1, Validators.required],
      email: ['', [Validators.required, Validators.email]],
  });
  this.order(this.route.snapshot.params.id);
  let domain=window.location.hostname
  this.value="https://"+domain+'/utilize/'+this.route.snapshot.params.id+'/';
  webGlObject1.loadQr2(this.value);
  }
  order(id){
    this.service.orderDetails(id).subscribe((res)=>{
      this.product=res;
      this.user= res.user
    })
  }
  get f() { return this.registerForm.controls; }
  onSubmit(){
    if (this.registerForm.invalid) {
      return;
  }
  this.router.navigate(['/utilize',this.route.snapshot.params.id])
  }
}

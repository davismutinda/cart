import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { CartService } from 'src/app/cart.service';
import { ServiceService } from 'src/app/service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  @ViewChild(ModalDirective, { static: false }) modal?: ModalDirective;
  count=0;
  products=[]
  panelOpenState = false;
  selected:any={};
  constructor(public service:CartService,public router:Router,public productService:ServiceService) { }

  ngOnInit(): void {
 
    this.productService.getProducts().subscribe((res)=>{
        this.products=res;
    })
  this.service.count.subscribe((res)=>{
    this.count = res;
  })


  }
  addCart(item){
    this.selected =item;

    if(item.enable_product_variant =='off'){
      this.selected.variant_options ={}
    this.service.addCart(this.selected);
    this.router.navigateByUrl('/payment')
    }else{
      this.modal.show();
    }
    
  }
  setVariant(group,name){
    let data ={
      variant_name:group,
      variant_options:name
    }
    this.selected.variant_options = data
     this.service.addCart(this.selected);
  }
  next(){
    if(this.selected.variant_options){
      this.router.navigateByUrl('/payment');
    }
  }
}

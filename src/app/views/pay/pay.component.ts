import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CartService } from 'src/app/cart.service';
import { ServiceService } from 'src/app/service.service';
import { SearchCountryField, CountryISO, PhoneNumberFormat } from 'ngx-intl-tel-input';
@Component({
  selector: 'app-pay',
  templateUrl: './pay.component.html',
  styleUrls: ['./pay.component.scss']
})
export class PayComponent implements OnInit {
  data:any={};
  cart=[];
  registerForm!: FormGroup;
  submitted!: boolean;
  shop='';
  cart_items=[];
  total=0;
  loading = false;
  count=0;
  id;
  max=15
  separateDialCode = false;
	SearchCountryField = SearchCountryField;
	CountryISO = CountryISO;
  PhoneNumberFormat = PhoneNumberFormat;
	preferredCountries: CountryISO[] = [CountryISO.UnitedStates, CountryISO.UnitedKingdom];
  constructor(private formBuilder: FormBuilder,private route: ActivatedRoute,public service:ServiceService,public toastr:ToastrService,public cartService:CartService,public router:Router) { }
  ngOnInit(): void {
    this.cartService.count.subscribe((res)=>{
      this.count = res;
    })
    this.cartService.cart_products.subscribe((res)=>{
      this.cart=res;
    })
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      phone: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
  });
  this.cart_items = JSON.parse(sessionStorage.getItem('items'))
  let amount=0;
  this.cart_items.forEach(element => {
    amount+=element.price
  });
  this.total = amount;
  }
  get f() { return this.registerForm.controls; }

  onSubmit() {
      if (this.registerForm.invalid) {
          return;
      }
      this.loading=true;
      this.submitted = true;
      sessionStorage.setItem('user',JSON.stringify(this.registerForm.value))
      let mobile = this.registerForm.get('phone').value
      sessionStorage.setItem('phone',mobile.e164Number.replace('+',"").trim())
      this.router.navigateByUrl('/checkout')
   
     
  }
  removeItem(dataObj){
  this.cartService.removeItem(dataObj);
  this.ngOnInit();
  }

  
}


import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ServiceService } from 'src/app/service.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  constructor(public route:Router,public service:ServiceService,public toastr:ToastrService) { }
  ngOnInit(): void {
    
  }
buyProduct(name: any,price: any){
let data={name:name,price:price}
sessionStorage.clear()
sessionStorage.setItem('item',JSON.stringify(data))
this.route.navigateByUrl('/pay')
}
}

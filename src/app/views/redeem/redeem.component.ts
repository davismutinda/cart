import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceService } from 'src/app/service.service';
declare var myExtObject: any;
@Component({
  selector: 'app-redeem',
  templateUrl: './redeem.component.html',
  styleUrls: ['./redeem.component.scss']
})
export class RedeemComponent implements OnInit {
  value: any;
  id: any;
  user:any={};
  shareableUrl="https://www.google.com/"
  product: any={};
  constructor(private route: ActivatedRoute,public router:Router,public service:ServiceService) { }
  ngOnInit(): void {
    let domain=window.location.hostname
    this.id = this.route.snapshot.params.id
    this.order(this.id);
    this.value="https://"+domain+'/collect/'+this.route.snapshot.params.id+'/';
    myExtObject.loadQr(this.value);
  }
  share(){
    let message = encodeURIComponent(this.value)
    window.open("https://api.whatsapp.com/send?text="+this.product.message+' to collect your prodcut click here'+message, "_blank");

  }
  order(id){
    this.service.orderDetails(id).subscribe((res)=>{
      this.product=res;
      // this.user= res.user
    })
  }
  mailto(emailAddress: string) {
    return "mailto:" + emailAddress + "?subject="+'Perfectly mixed moment from me' +'&body='+this.product.message+' to collect your product click here '+' '+this.value
}
}

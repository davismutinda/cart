import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CartService } from 'src/app/cart.service';
import { ServiceService } from 'src/app/service.service';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.scss']
})
export class StoreComponent implements OnInit {
  count=0;
  products=[]
  store:any={};
  constructor(public service:CartService,public router:Router,public productService:ServiceService,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.productService.storeProducts(this.route.snapshot.params.id).subscribe((res)=>{
        this.products=res.products;
        this.store=res.store_details
    })
  this.service.count.subscribe((res)=>{
    this.count = res;
  })


  }
  addCart(item){
    this.service.addCart(item);
    this.router.navigateByUrl('/payment')
  }
  
}

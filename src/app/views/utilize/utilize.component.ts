import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceService } from 'src/app/service.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
declare var webGlObject: any;

@Component({
  selector: 'app-utilize',
  templateUrl: './utilize.component.html',
  styleUrls: ['./utilize.component.scss']
})
export class UtilizeComponent implements OnInit {
  product: any=[];
  user: any={};
  id: any;
  value: string;
  registerForm: FormGroup;
  submitted=false;
  constructor(private route: ActivatedRoute,public router:Router,public service:ServiceService,private formBuilder: FormBuilder) { }
  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      password: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
  });
    let domain=window.location.hostname
    this.id = this.route.snapshot.params.id
    this.order(this.id);
    this.value="https://"+domain+'/utilize/'+this.route.snapshot.params.id+'/';
    webGlObject.loadQr1(this.value);
  }
    get f() { return this.registerForm.controls; }
  order(id){
    this.service.orderDetails(id).subscribe((res)=>{
      if(res.payment_status==1){
        this.product=res;
      }else{
        this.router.navigate(['/payment'])
      }
     
    })
  }
  onSubmit(){

  }
}
